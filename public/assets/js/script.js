function addNumbers() {
	let num1 = Number(document.getElementById('num1').value);
	let num2 = Number(document.getElementById('num2').value);

	document.getElementById('result').innerHTML = num1 + num2;
	return false;
}

function subtractNumbers() {
	let num1 = Number(document.getElementById('num1').value);
	let num2 = Number(document.getElementById('num2').value);

	document.getElementById('result').innerHTML = num1 - num2;
	return false;
}

function multiplyNumbers() {
	let num1 = Number(document.getElementById('num1').value);
	let num2 = Number(document.getElementById('num2').value);

	document.getElementById('result').innerHTML = num1 * num2;
	return false;
}

function divideNumbers() {
	let num1 = Number(document.getElementById('num1').value);
	let num2 = Number(document.getElementById('num2').value);

	document.getElementById('result').innerHTML = num1 / num2;
	return false;
}

function modulusNumbers() {
	let num1 = Number(document.getElementById('num1').value);
	let num2 = Number(document.getElementById('num2').value);

	document.getElementById('result').innerHTML = num1 % num2;
	return false;
}

function clearNumbers() {
	let num1 = Number(document.getElementById('num1').value);
	let num2 = Number(document.getElementById('num2').value);
	
	document.getElementById('result').innerHTML = '';
	document.getElementById('num1').value = '';
	document.getElementById('num2').value = '';
}

document.getElementById('add').addEventListener('click', addNumbers);
document.getElementById('subtract').addEventListener('click', subtractNumbers);
document.getElementById('multiply').addEventListener('click', multiplyNumbers);
document.getElementById('divide').addEventListener('click', divideNumbers);
document.getElementById('modulus').addEventListener('click', modulusNumbers);
document.getElementById('erase').addEventListener('click', clearNumbers);
